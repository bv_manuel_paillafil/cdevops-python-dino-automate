from PIL import ImageGrab, ImageOps
import pyautogui
import time
import numpy

#declaracion de variables auxiliares
auxPixelesColores = 0
#coordenadas para obtener screenshot
x = 300
y = 585
#tamaño del screenshot
width  = 200
height = 95

pyautogui.hotkey('win') #presiona tecla windows
time.sleep(2) #espera 2segundos
pyautogui.typewrite('Chrome') #tipea Chrome
time.sleep(1) #espera 1segundo
pyautogui.hotkey('enter') #presiona tecla enter
time.sleep(3) #espera 3segundos
pyautogui.typewrite('chrome://dino') #chrome queda por defecto el input para url
pyautogui.hotkey('enter') #presiona tecla enter
time.sleep(2) #espera 2segundos
pyautogui.hotkey('space') #presiona tecla espacio para comenzar el juego
time.sleep(2) #espera 2segundos

while True:
    # saca screenshot segun coordenadas
    imagenBN = ImageGrab.grab(bbox =(x, y, x+width, y+height))
    # convierte la imagen a escala de grises
    imagenBN = imagenBN.convert("1")
    # cuenta los pixeles asociados a los colores
    # en este caso blanco y negro, contara 2 colores
    #   * 255: es blanco
    #   * 0  : es negro
    arr = imagenBN.getcolors()
    print(arr)

    # entra al bloque si hay cambios en el screenshot obtenido
    if(auxPixelesColores != arr[0][0]):
        # verifica si el screenshot tiene pixeles negros
        if(arr[0][1] == 0):
            print("negro!!!")
            #si hay mas de 1000 pixeles negros: SALTA!
            if(arr[0][0] > 500):
                print("¡¡¡SALTA!!!")
                pyautogui.hotkey('space')
            else:
                print("no saltes: falsa alarma")

    # variable auxiliar
    auxPixelesColores = arr[0][0]
    # espera 0.1s
    time.sleep(0.05)  